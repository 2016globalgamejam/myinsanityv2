//
//  File.swift
//  MyInsanity
//
//  Created by Cape Crow on 1/30/16.
//  Copyright © 2016 TheCodingArt. All rights reserved.
//

import Foundation
import GameplayKit
import SpriteKit

enum ItemType {
    case Test
    case Wood
    case Stone
    
    func texture() -> SKTexture {
        switch self {
        case .Test: return SKTexture(imageNamed: "goblin")
        case .Wood: return SKTexture(imageNamed: "obj_log01")
        case .Stone: return SKTexture(imageNamed: "rock_one")
        }
    }
    
}

class InventoryItemEntity: GKEntity {
    
    // MARK: - Header
    var spriteComponent: SpriteComponent!
    private var itemQuantity = 0
    var quantity: Int {
        get {
            return itemQuantity
        }
        set(value) {
            labelNode.text = ":\(value)"
            labelNode.position = CGPoint(x: spriteComponent.spriteNode.size.width - 25, y: -spriteComponent.spriteNode.size.width/2)
            
            itemQuantity = value
        }
    }
    
    private var labelNode = SKLabelNode(fontNamed:"Chalkduster")
    
    var itemType: ItemType
    
    var disNode: SKNode?
    
    // MARK: - Overrides
    init(type: ItemType) {
        itemType = type
        super.init()
        
        let texture = type.texture()
        spriteComponent = SpriteComponent(parentEntity: self, texture: texture, size: texture.size())
        
        addComponent(spriteComponent)
        
    }
    
    // MARK: - Function
    func calculatedWidth() -> CGFloat {
        return spriteComponent.spriteNode.size.width + labelNode.frame.size.width
    }
    
    func displayNode() -> SKNode {
        guard disNode == nil else { return disNode! }
        
        let texture = itemType.texture()
        spriteComponent = SpriteComponent(parentEntity: self, texture: texture, size: texture.size())
        
        let node = SKNode()
        labelNode.fontSize = 45
        labelNode.horizontalAlignmentMode = .Left
        labelNode.position = CGPoint(x: spriteComponent.spriteNode.size.width - 20, y: -spriteComponent.spriteNode.size.height/2)
        
        node.addChild(spriteComponent.spriteNode)
        node.addChild(labelNode)
        
        disNode = node
        return disNode!
    }
    
}