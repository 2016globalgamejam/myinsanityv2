//
//  EndGameState.swift
//  MyInsanity
//
//  Created by Brandon Levasseur on 1/30/16.
//  Copyright © 2016 TheCodingArt. All rights reserved.
//

import SpriteKit
import GameplayKit

class EndGameState: GKState {

    unowned let scene: GameScene
    
    init(gameScene: GameScene) {
        scene = gameScene
        
    }
    
}
