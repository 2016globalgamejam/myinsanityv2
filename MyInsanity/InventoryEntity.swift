//
//  InventoryEntity.swift
//  MyInsanity
//
//  Created by Cape Crow on 1/30/16.
//  Copyright © 2016 TheCodingArt. All rights reserved.
//

import Foundation
import SpriteKit
import GameplayKit

class InventoryEntity: GKEntity {
    
    // MARK: - Header
    var items = [InventoryItemEntity]()
    var displayNode = SKNode()
    
    // MARK: - Function
    func addItem(inventoryItem: InventoryItemEntity, count: Int = 1) {
        
        for nextItem in items {
            if inventoryItem.itemType == nextItem.itemType {
                nextItem.quantity += count
                
                updateItemPositions()
                
                return
            }
        }
        
        inventoryItem.quantity = 1
        let itemDisplayNode = inventoryItem.displayNode()
        
        items.append(inventoryItem)
        displayNode.addChild(itemDisplayNode)
        
        updateItemPositions()
        
    }
    
    func removeItem(inventoryItem: InventoryItemEntity, count: Int = 1) {
        
        guard let index = items.indexOf(inventoryItem) else { return }
        
        inventoryItem.quantity -= count
        
        if inventoryItem.quantity <= 0 {
            items.removeAtIndex(index)
        }
        
    }
    
    func numberOfItemsForItemType(itemType: ItemType) -> Int? {
        for inventoryItem in items {
            if inventoryItem.itemType == itemType {
                return inventoryItem.quantity
            }
        }
        
        return nil
    }
    
    func updateItemPositions() {
        var calculatedWidth = CGFloat(0)
        
        for item in items {
            item.displayNode().position = CGPoint(x: calculatedWidth, y: 0.0)
            calculatedWidth -= item.calculatedWidth()
        }
    }
    
}