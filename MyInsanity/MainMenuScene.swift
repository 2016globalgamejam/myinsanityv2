//
//  MainMenuScene.swift
//  MyInsanity
//
//  Created by Cape Crow on 1/30/16.
//  Copyright © 2016 TheCodingArt. All rights reserved.
//

import Foundation
import SpriteKit

class MainMenuScene: SKScene {
    
    // MARK: - Overides
    override func didMoveToView(view: SKView) {
        
        SKTAudio.sharedInstance().playBackgroundMusic("Waiting.mp3")
        
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        sceneTapped()
    }
    
    
    // MARK: - Functions
    func sceneTapped() {
        
        let gameScene = GameScene(fileNamed: "GameScene")!
        gameScene.scaleMode = scaleMode
        
        let doorwayTransition = SKTransition.fadeWithDuration(1)
        view?.presentScene(gameScene, transition: doorwayTransition)
        
    }
    
}