//
//  SpriteComponent.swift
//  MyInsanity
//
//  Created by Brandon Levasseur on 1/30/16.
//  Copyright © 2016 TheCodingArt. All rights reserved.
//

import SpriteKit
import GameplayKit

class SpriteComponent: GKComponent {
    let spriteNode: SpriteNode
    
    init(parentEntity: GKEntity, texture: SKTexture, size: CGSize) {
        spriteNode = SpriteNode(associatedEntity: parentEntity, texture: texture, size: size)
    }

}
    