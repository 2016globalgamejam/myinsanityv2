//
//  PlayerEntity.swift
//  MyInsanity
//
//  Created by Brandon Levasseur on 1/30/16.
//  Copyright © 2016 TheCodingArt. All rights reserved.
//

import SpriteKit
import GameplayKit

class PlayerEntity: GKEntity {
    var spriteComponent: SpriteComponent!
    var animationComponent: AnimationComponent!
    var movementComponent: MovementComponent!
    
    override init() {
        super.init()
        
        let playerTexture = SKTexture(imageNamed: "Idle_Down_0")
        spriteComponent = SpriteComponent(parentEntity: self, texture: playerTexture, size: CGSize(width: 32, height: 32))
        spriteComponent.spriteNode.zPosition = 10
        addComponent(spriteComponent)
        
        movementComponent = MovementComponent(pointsPerSecond: 480)
        addComponent(movementComponent)
        
        let physicsBody = SKPhysicsBody(circleOfRadius: 15)
        physicsBody.dynamic = true
        physicsBody.allowsRotation = false
        physicsBody.categoryBitMask = PhysicsMaskType.Player.rawValue
        physicsBody.collisionBitMask = PhysicsMaskType.Edge.rawValue
        physicsBody.contactTestBitMask = PhysicsMaskType.Log.rawValue
        
        spriteComponent.spriteNode.physicsBody = physicsBody
        
        animationComponent = AnimationComponent(node: spriteComponent.spriteNode, textureSize: spriteComponent.spriteNode.size, animations: loadAnimations())
        addComponent(animationComponent)
        
    }
    
    func loadAnimations() -> [AnimationState : Animation] {
        
        let textureAtlas = SKTextureAtlas(named: "Profile")
        var animations = [AnimationState: Animation]()
        let animationStates = [AnimationState.WalkingUp, .WalkingDown, .WalkingLeft, .WalkingRight, .IdleDown, .IdleLeft, .IdleRight, .IdleUp]
        
        for animationState in animationStates {
            animations[animationState] = AnimationComponent.animationFromAtlas(textureAtlas, withImageIdentifier: animationState.rawValue, forAnimationState: animationState)
        }
        return animations
    }
    
    
}
