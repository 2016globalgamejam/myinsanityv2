//
//  MoveComponent.swift
//  MyInsanity
//
//  Created by Brandon Levasseur on 1/30/16.
//  Copyright © 2016 TheCodingArt. All rights reserved.
//

import SpriteKit
import GameplayKit


enum LastDirection {
    case Left
    case Right
    case Up
    case Down
}

class MovementComponent: GKComponent {
    var velocity = CGPoint.zero
    var movePointsPerSecond: CGFloat //= CGFloat(480.0)
    var lastDirection = LastDirection.Down
    var animationComponent: AnimationComponent {
        guard let animationComponent = entity?.componentForClass(AnimationComponent.self) else {
            fatalError("A MovementComponent's entity must have an animationComponent")
        }
        return animationComponent
    }
    
    var targetLocation: CGPoint? {
        didSet {
            var offset = targetLocation! - spriteComponent.spriteNode.position
            let direction = offset.normalize()
            velocity = direction * movePointsPerSecond
        }
    }
    var spriteComponent: SpriteComponent {
        guard let spriteComponent = entity?.componentForClass(SpriteComponent.self) else {
            fatalError("We need")
        }
        return spriteComponent
    }
    
    init(pointsPerSecond: CGFloat) {
        movePointsPerSecond = pointsPerSecond
    }
    
    override func updateWithDeltaTime(seconds: NSTimeInterval) {
        if let destinationLocation = targetLocation {
            let spriteNode = spriteComponent.spriteNode
            
            let delta = destinationLocation - spriteNode.position
            if delta.length() <= movePointsPerSecond * CGFloat(seconds) {
                spriteNode.position = destinationLocation
                velocity = CGPoint.zero
            } else {
                moveSprite(spriteNode, velocity: velocity, deltaTime: seconds)
            }
            
            
            switch velocity.angle {
            case 0: break//Left empty on purpose to break switch if there is no angle
            case CGFloat(45).degreesToRadians() ..< CGFloat(135).degreesToRadians():
                animationComponent.requestedAnimationState = .WalkingUp
                lastDirection = .Up
            
            case CGFloat(-135).degreesToRadians() ..< CGFloat(-45).degreesToRadians():
                animationComponent.requestedAnimationState = .WalkingDown
                lastDirection = .Down
                
            case CGFloat(-45).degreesToRadians() ..< CGFloat(45).degreesToRadians():
                animationComponent.requestedAnimationState = .WalkingRight
                lastDirection = .Right
                
            case CGFloat(-180).degreesToRadians() ..< CGFloat(-135).degreesToRadians():
                animationComponent.requestedAnimationState = .WalkingLeft
                lastDirection = .Left
                
            case CGFloat(135).degreesToRadians() ..< CGFloat(180).degreesToRadians():
                animationComponent.requestedAnimationState = .WalkingLeft
                lastDirection = .Left
                
            default: break
            }
            
            if velocity == CGPoint.zero {
                switch lastDirection {
                case .Up:
                    animationComponent.requestedAnimationState = .IdleUp
                    
                case .Down:
                    animationComponent.requestedAnimationState = .IdleDown
                    
                case .Right:
                    animationComponent.requestedAnimationState = .IdleRight
                    
                case .Left:
                    animationComponent.requestedAnimationState = .IdleLeft
                    
                } }
            
        }
        
        
    }
    
    func moveSprite(spriteNode: SKSpriteNode, velocity: CGPoint, deltaTime: NSTimeInterval) {
        let deltaLocation = velocity * CGFloat(deltaTime)
        spriteNode.position += deltaLocation
    }
}
