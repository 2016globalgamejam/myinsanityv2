//
//  AnimationComponent.swift
//  MyInsanity
//
//  Created by Brandon Levasseur on 1/30/16.
//  Copyright © 2016 TheCodingArt. All rights reserved.
//

import SpriteKit
import GameplayKit

enum AnimationState: String {
    case WalkingLeft = "Walk_Left"
    case WalkingRight = "Walk_Right"
    case WalkingUp = "Walk_Up"
    case WalkingDown = "Walk_Down"
    case IdleUp = "Idle_Up"
    case IdleDown = "Idle_Down"
    case IdleRight = "Idle_Right"
    case IdleLeft = "Idle_Left"
}

struct Animation {
    let animationState: AnimationState
    let textures: [SKTexture]
    let repeatAnimationForEver: Bool
}

class AnimationComponent: GKComponent {
    
    static let animationKey = "Animation"
    static let animationTimePerFrame = NSTimeInterval(1.0 / 30.0)
    
    let spriteNode: SKSpriteNode
    
    var animations: [AnimationState : Animation]
    
    private(set) var currentAnimation: Animation?
    var requestedAnimationState: AnimationState?
    
    init(node: SKSpriteNode, textureSize: CGSize, animations: [AnimationState : Animation]) {
        spriteNode = node
        self.animations = animations
        
        super.init()
    }
    
    func runAnimationForAnimationState(animationState: AnimationState) {
        if currentAnimation != nil && currentAnimation!.animationState == animationState {
            // we are undergoing the current animation
            return
        }
        
        guard let animation = animations[animationState] else {
            print("unknown animation for state \(animationState.rawValue)")
            return
        }
        
        // so stupid i couldn't directly call the animation key from with in it's own class -.-
        spriteNode.removeActionForKey(AnimationComponent.animationKey)
        
        let texturesAction: SKAction
        if animation.repeatAnimationForEver {
            texturesAction = SKAction.repeatActionForever(SKAction.animateWithTextures(animation.textures, timePerFrame: AnimationComponent.animationTimePerFrame))
        } else {
            texturesAction = SKAction.animateWithTextures(animation.textures, timePerFrame: AnimationComponent.animationTimePerFrame)
        }
        
        
        spriteNode.runAction(texturesAction, withKey: AnimationComponent.animationKey)
        
        currentAnimation = animation
        
    }
    
    override func updateWithDeltaTime(seconds: NSTimeInterval) {
        super.updateWithDeltaTime(seconds)
        
        if let animationState = requestedAnimationState {
            runAnimationForAnimationState(animationState)
            requestedAnimationState = nil
        }
    }
    
    
    class func animationFromAtlas(atlas: SKTextureAtlas, withImageIdentifier identifier: String, forAnimationState animationState: AnimationState, repeatAnimationForEver: Bool = true) -> Animation {
        let textures = atlas.textureNames.filter {
            $0.hasPrefix("\(identifier)_") }.sort { $0 < $1 }.map {
                atlas.textureNamed($0)
                
        }
        
        return Animation(animationState: animationState, textures: textures, repeatAnimationForEver: repeatAnimationForEver)
        
    }
}


