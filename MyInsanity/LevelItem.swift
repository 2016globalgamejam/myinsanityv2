//
//  LevelItem.swift
//  MyInsanity
//
//  Created by Zephaniah Cohen on 1/30/16.
//  Copyright © 2016 TheCodingArt. All rights reserved.
//

import Foundation
import SpriteKit


class LevelItem : SKNode {
    
    var locationInWorld : CGPoint!
    
    //MARK: Init
    override init(){
        super.init()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: Sprite Initialization
    
    func buidLevelItemSpriteWithNameAndPosition(spriteImageName : String , position : CGPoint){
        let texture = SKTexture(imageNamed: spriteImageName)
        buildSpriteFromTexture(texture, position: position)
    }
    
    func buildLevelItemForPositionWithRandomSprite(position : CGPoint) {
        
        var randomTexture : SKTexture?
    
        let randomNumber = Int.random(min: 1, max: 10)
        
        if randomNumber%2 == 0 {
            randomTexture = SKTexture(imageNamed: "tree_one")
        } else {
            
            let randomNumberRollTwo = Int.random(min: 1, max: 10)
            
            if randomNumberRollTwo%2 == 0{
                   randomTexture = SKTexture(imageNamed: "log_one")
            } else {
                randomTexture = SKTexture(imageNamed: "rock_two")
            }
        }
        
        buildSpriteFromTexture(randomTexture!, position: position)
    }
    
    func buildSpriteFromTexture(texture : SKTexture, position : CGPoint){
        let backgroundSprite = SKSpriteNode(texture: texture, color: SKColor.clearColor(), size: texture.size())
        self.position = position
        zPosition = 1
        addChild(backgroundSprite)
    }
}