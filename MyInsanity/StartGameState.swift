//
//  StartGameState.swift
//  MyInsanity
//
//  Created by Brandon Levasseur on 1/30/16.
//  Copyright © 2016 TheCodingArt. All rights reserved.
//

import SpriteKit
import GameplayKit

class StartGameState: GKState {

    unowned let scene: GameScene
    
    init(gameScene: GameScene) {
        scene = gameScene
        
    }
    
    override func didEnterWithPreviousState(previousState: GKState?) {
        SKTAudio.sharedInstance().playBackgroundMusic("Wandering (Demo).mp3")
    }
    
}
