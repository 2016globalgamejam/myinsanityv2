//
//  EntityNode.swift
//  MyInsanity
//
//  Created by Cape Crow on 1/30/16.
//  Copyright © 2016 TheCodingArt. All rights reserved.
//

import Foundation
import GameplayKit
import SpriteKit

class SpriteNode: SKSpriteNode {
    weak var entity: GKEntity!
    
    init(associatedEntity: GKEntity, texture: SKTexture?, color: UIColor = UIColor.whiteColor(), size: CGSize) {
        
        super.init(texture: texture, color: color, size: size)

        entity = associatedEntity
        

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("Nope")
    }
}