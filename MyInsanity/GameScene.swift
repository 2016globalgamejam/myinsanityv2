//
//  GameScene.swift
//  MyInsanity
//
//  Created by Brandon Levasseur on 1/30/16.
//  Copyright (c) 2016 TheCodingArt. All rights reserved.
//

import SpriteKit
import GameplayKit

struct PhysicsMaskType: OptionSetType {
    
    static let Player   = PhysicsMaskType(rawValue: 0b1)
    static let Tree     = PhysicsMaskType(rawValue: 0b10)
    static let Log      = PhysicsMaskType(rawValue: 0b100)
    static let Wall     = PhysicsMaskType(rawValue: 0b1000)
    static let Edge     = PhysicsMaskType(rawValue: 0b10000)
    
    let rawValue: UInt32
    
    init(rawValue: UInt32) {
        self.rawValue = rawValue
    }
    
}

class GameScene: SKScene {

    var worldLayer: SKNode!
    var backgroundLayer: SKNode!
    var foregroundLayer: SKNode!
    
    var entities = Set<GKEntity>()
    
    var lastUpdateTimeInterval = NSTimeInterval(0)
    let maximumUpdateDeltaTime = NSTimeInterval(1.0 / 60.0)
    var lastDeltaTime = NSTimeInterval(0)
    
    var lastTouchLocation: CGPoint?
    
    var playerEntity: PlayerEntity!
    var inventory = InventoryEntity()
    
//    var sceneOverlap: CGFloat {
//        guard let view = self.view else {
//            return 0
//        }
//        
//        let scale = view.bounds.width / size.width
//        let scaledHeight = size.height * scale
//        let scaledOverlap = scaledHeight - view.bounds.height
//        return scaledOverlap / scale
//    }
    
    var cameraPosition: CGPoint {
        
        get {
            return CGPoint(x: camera!.position.x, y: camera!.position.y)
        }
        
        set {
            
            let cameraScale = CGFloat(0.44)
            let sceneScale = 1 - cameraScale//CGFloat(1 - 0.44  + 0.05 /*possible rounding error adjustment*/)
//            let viewAspectRatio = CGRectGetWidth(view!.frame)/CGRectGetHeight(view!.frame)
            let newPositionValue = double2(x: Double(newValue.x * sceneScale), y: Double(newValue.y * sceneScale))
            
            let scaledSceneSize = CGSize(width: size.width * sceneScale , height: size.height * sceneScale)
////            scaledSceneSize.height = scaledSceneSize.height / viewAspectRatio
            
            let cameraSize = view!.bounds.size
            let scaledCameraSize = CGSize(width: cameraSize.width * cameraScale, height: cameraSize.height * cameraScale)
            
            
            let minX = -scaledSceneSize.width * anchorPoint.x + scaledCameraSize.width / 2
            let minY = -scaledSceneSize.height * anchorPoint.y + scaledCameraSize.height / 2
            
            let minValues = double2(x: Double(minX), y: Double(minY))
            
            let maxX = (scaledSceneSize.width * anchorPoint.x - scaledCameraSize.width / 2) //size.width - cameraSize.width / 2
            let maxY = (scaledSceneSize.height * anchorPoint.y - scaledCameraSize.height / 2) //- cameraSize.height / 2
            
            let maxValues = double2(x: Double(maxX), y: Double(maxY))
            
            let clampedPosition = clamp(newPositionValue, min: minValues, max: maxValues)
            
            
            camera!.position = CGPoint(x: (clampedPosition.x / Double(sceneScale)), y: (clampedPosition.y / Double(sceneScale)))
        }
        
    }
    
    lazy var stateMachine: GKStateMachine = GKStateMachine(states: [
        StartGameState(gameScene: self),
        InsanityState0(gameScene: self),
        InsanityState1(gameScene: self)
        ])
    
    lazy var componentSystems: [GKComponentSystem] = {
        let animationSystem = GKComponentSystem(componentClass: AnimationComponent.self)
        let movementSystem = GKComponentSystem(componentClass: MovementComponent.self)
        return [animationSystem, movementSystem]
    }()
    
    override func didMoveToView(view: SKView) {
        
        worldLayer = childNodeWithName("World")
        backgroundLayer = worldLayer.childNodeWithName("Background")
        foregroundLayer = worldLayer.childNodeWithName("Foreground")
        
        let cameraNode = SKCameraNode()
        cameraNode.position = CGPoint(x: size.width/2, y: size.width/2)
        cameraNode.setScale(0.44)
        addChild(cameraNode)
        camera = cameraNode
        
        
        physicsWorld.contactDelegate = self
        physicsWorld.gravity = CGVector.zero
        
        spawnPlayer()
        
        addChild(inventory.displayNode)
        
//        debugDrawPlayableArea()
        
        stateMachine.enterState(StartGameState.self)
        
        let worldPhysics = SKPhysicsBody(edgeLoopFromRect: backgroundLayer.calculateAccumulatedFrame())
        worldPhysics.categoryBitMask = PhysicsMaskType.Edge.rawValue
        worldPhysics.dynamic = false
        foregroundLayer.physicsBody = worldPhysics

    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        guard let touch = touches.first else {
            return
        }
        
        let touchLocation = touch.locationInNode(self)
        
        if let playerNode = foregroundLayer.childNodeWithName("Player") as? SpriteNode, let playerEntity = playerNode.entity as? PlayerEntity  {
            
            let adjustedLocation = convertPoint(touchLocation, toNode: foregroundLayer)
            
            playerEntity.movementComponent.targetLocation = adjustedLocation
        }
    }
   
    override func update(currentTime: CFTimeInterval) {
        var deltaTime = currentTime - lastUpdateTimeInterval
        deltaTime = deltaTime > maximumUpdateDeltaTime ? maximumUpdateDeltaTime : deltaTime
        lastUpdateTimeInterval = currentTime
        
        for componentSystem in componentSystems {
            componentSystem.updateWithDeltaTime(deltaTime)
        }
        
        if let player = foregroundLayer.childNodeWithName("Player") as? SpriteNode {
            let cameraPosition = convertPoint(player.position, fromNode: foregroundLayer)
            
            centerCameraOnPoint(cameraPosition)
        }
    }
    
    func spawnPlayer() {
        
        let spawnNode = foregroundLayer.childNodeWithName("SpawnPlaceholder")!
        let spawnLocation = spawnNode.position
        spawnNode.removeFromParent()
        
        let playerEntity = PlayerEntity()
        let playerSpriteNode = playerEntity.spriteComponent.spriteNode
        playerSpriteNode.name = "Player"
        playerSpriteNode.position = spawnLocation
        playerSpriteNode.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        addEntity(playerEntity)
        
        let cameraPosition = convertPoint(playerSpriteNode.position, fromNode: foregroundLayer)
        
        centerCameraOnPoint(cameraPosition)
        
        self.playerEntity = playerEntity
        
        self.playerEntity.animationComponent.requestedAnimationState = .WalkingLeft
        
    }
    
    func addEntity(entity: GKEntity) {
        entities.insert(entity)
        
        if let spriteNode = entity.componentForClass(SpriteComponent.self)?.spriteNode {
            foregroundLayer.addChild(spriteNode)
        }
        
        for componentSystem in componentSystems {
            componentSystem.addComponentWithEntity(entity)
        }

    }
    
    func addItemToInventory(item: ItemType, quantity: Int) {
        let itemToAdd = InventoryItemEntity(type: item)
        inventory.addItem(itemToAdd, count: quantity)
    }
    
    func centerCameraOnPoint(point: CGPoint) {
        guard let _ = camera else {
            return
        }
        
        cameraPosition = point//camera.position = point//cameraPosition = point
    }
    
    func debugDrawPlayableArea() {
        let shape = SKShapeNode()
        let path = CGPathCreateMutable()
        CGPathAddRect(path, nil, playerEntity.spriteComponent.spriteNode.frame)
        shape.path = path
        shape.strokeColor = SKColor.redColor()
        shape.lineWidth = 4.0
        shape.zPosition = 100
        addChild(shape)
    }
    
}

extension GameScene: SKPhysicsContactDelegate {
    func didBeginContact(contact: SKPhysicsContact) {
        
        let collision = contact.bodyA.categoryBitMask | contact.bodyB.categoryBitMask
        
        switch collision {
        case PhysicsMaskType.Player.union(.Log).rawValue:
            addItemToInventory(.Wood, quantity: 1)
            // need to remove the node that was intersected with the player
            
        default:
            break
        }
        
    }
}
